package com.example.practica2_imc

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.practica2_imc.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding:ActivityMainBinding

    @SuppressLint("ResourceAsColor")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_main)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val indice1 = getString(R.string.imc1)
        val indice2 = getString(R.string.imc2)
        val indice3 = getString(R.string.imc3)
        val indice4 = getString(R.string.imc4)

        binding.btnCalcular.setOnClickListener {
            val peso = binding.txtPeso.text.toString().toInt()
            val estatura = binding.txtEstatura.text.toString().toDouble()
            val imc = peso / (estatura * 2)
            binding.txtIMC.text = imc.toString()
            if(imc <= 18.5){
                binding.txtIndice.text = "$indice1"
                binding.img.setImageResource(R.drawable.desnutrido)
            }
            if(imc > 18.5 && imc <= 24.9){
                binding.txtIndice.text = "$indice2"
                binding.img.setImageResource(R.drawable.pesobajo)
            }
            if(imc > 24.9 && imc <= 29.9){
                binding.txtIndice.text = "$indice3"
                binding.img.setImageResource(R.drawable.pesonormal)
            }
            if(imc > 29.9){
                binding.txtIndice.text = "$indice4"
                binding.img.setImageResource(R.drawable.sobrepeso)
            }

        }
    }
}